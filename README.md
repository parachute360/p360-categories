# README #

This repository contains the JSON content that describes all categories and custom properties (if any) utilized by the User Portal. There are separate files for Personal and Professional subscriptions. The JSON files are retrieved by the User Portal web application at startup.
